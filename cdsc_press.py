import lxml.html
import urllib.request
import psycopg2
import hashlib
import telegram_bot


content = urllib.request.urlopen("https://www.cdscnp.com/Home/pressrelease").read()
doc = lxml.html.fromstring(content)
home_content = doc.xpath('//*[@id="dynamic-table"]')
allRows = home_content[0][1][:]

con = psycopg2.connect(
    database = "test",
    user = "kaazisaheb"
)

cur = con.cursor()
cur.execute('''
create table if not exists cdsc_news(
title varchar(500),
date varchar(50), 
attachment varchar(200),
hashed char(64) unique
);''')

for i, tr in enumerate(allRows):
    resp = {} 
    resp['Title'] = tr[1].text_content().strip()  
    resp['Date'] = tr[2].text_content().strip()   
    resp['Attachment'] = tr[1][0].get('href')
    resp['hashed'] = hashlib.sha256(tr.text_content().encode()).hexdigest()
    try:
        cur.execute(
                "insert into cdsc_news values (%s, %s, %s, %s)",
                tuple(resp.values())
            )

        telegram_bot.sendtext("*{0}*\n_{1}_\nAttachment:{2}".format(
            resp['Title'],
            resp['Date'],
            resp['Attachment']
        ))

        con.commit()
    
    except psycopg2.errors.UniqueViolation as e:
        error = e.pgcode
        con.rollback()
        continue

con.commit()
con.close()
exit() 
