import lxml.html
import urllib.request
import psycopg2
import hashlib
import telegram_bot


content = urllib.request.urlopen("https://nib.gov.np/directive").read()
doc = lxml.html.fromstring(content)
home_content = doc.xpath('//*[@id="post-194"]')
data = home_content[0][:]


con = psycopg2.connect(
    database = "test",
    user = "kaazisaheb"
)

cur = con.cursor()
cur.execute('''
create table if not exists nib_directive(
release varchar(500),
attachment varchar(200),
hashed char(64) unique
);''')

for i , h3 in enumerate(data):
    resp = {}
    try:
        if h3[0].tag.__str__() == "a":
            resp['release'] = h3[0].text_content().strip() 
            resp["Attachment"] = h3[0].get("href")
            resp['hashed'] = hashlib.sha256(h3.text_content().encode()).hexdigest()
            
    except IndexError as e:
        continue

    if(bool(resp)):
        try:
            cur.execute(
            "insert into nib_directive values (%s, %s, %s)",
                tuple(resp.values())
                )

            telegram_bot.sendtext("*{0}*\nAttachment:\n{1}".format(
                resp['release'],
                resp['Attachment']
                ))

            con.commit()
            
        except psycopg2.errors.UniqueViolation as e:
            error = e.pgcode
            con.rollback()
            continue

con.commit()
con.close()
exit()
    