import lxml.html
import urllib.request
import psycopg2
import hashlib
import telegram_bot


content = urllib.request.urlopen("http://www.nepalstock.com.np/news/category/0").read() 
doc = lxml.html.fromstring(content)
home_contents = doc.xpath('//div[@id="home-contents"]')[0]
allRows = home_contents[3][0][:]

con = psycopg2.connect(
    database = "test",
    user = "refugee"
)

cur = con.cursor()
cur.execute('''
create table if not exists nep_stock_news(
news_date varchar(50), 
company_symbol varchar(7),
news_title varchar(100), 
news_description varchar(500), 
attachment varchar(200),
hashed char(64) unique
);''')

for i, tr in enumerate(allRows):
    try:
        if len(tr[4]) < 3: 
            continue
    except:
        break

    innerRows = tr[4][1][0][1][1][0][:]

    resp = {} 
    for tr in innerRows: 
        if len(tr[1]) == 1 and tr[1][0].tag.__str__() == 'a':
            resp[tr[0].text_content()] = tr[1][0].get('href')
        else:
            resp[tr[0].text_content()] = tr[1].text_content().strip()
    resp['Attachment'] = resp.get('Attachment')
    resp['hashed'] =  hashlib.sha256(resp[tr[0].text_content()].encode('utf-8')).hexdigest()

    try:
        cur.execute(
                "insert into nep_stock_news values (%s, %s, %s, %s, %s, %s)",
                tuple(resp.values())
            ) 
        telegram_bot.sendtext("*{0}*\n_{1}_\nDescription:\n{2}\nCompany Symbol: {3}\nAttachment:\n{4}".format(
            resp['News Title'],
            resp['News Date'],
            resp['News Description'],
            resp['Company Symbol'],
            resp['Attachment']
        ))
        con.commit()
        
    except psycopg2.errors.UniqueViolation as e:
        error = e.pgcode
        con.rollback()
        continue

con.commit()
con.close()
exit()
