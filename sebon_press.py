import lxml.html
import urllib.request
import psycopg2
import hashlib
import telegram_bot


content = urllib.request.urlopen("https://www.sebon.gov.np/press-releases").read()
doc = lxml.html.fromstring(content)
home_contents = doc.xpath('/html/body/section[1]/div/div/div[2]/div[2]/div[3]')
allRows = home_contents[0][0][1][:]

con = psycopg2.connect(
    database = "test",
    user = "kaazisaheb"
)

cur = con.cursor()
cur.execute('''
create table if not exists sebon_press(
title varchar(500),
date varchar(50), 
attachment varchar(200),
hashed char(64) unique
);''')

for i, tr in enumerate(allRows):
    resp = {} 
    resp['Title'] = tr[0].text_content()  
    resp['Date'] = tr[1].text_content()   
    if len(tr[3]) == 1 and tr[3][0].tag.__str__() == 'a':
        resp['Attachment'] = tr[3][0].get('href')
    elif len(tr[2]) == 1 and tr[2][0].tag.__str__() == 'a':
        resp['Attachment'] = tr[2][0].get('href')
    else:
        resp['Attachment'] = "  None"
    
    resp['hashed'] = hashlib.sha256(tr.text_content().encode()).hexdigest()                                                                                                                                                                                                                                                                                                                                                                                                                                                     
    try:
        cur.execute(
                "insert into sebon_press values (%s, %s, %s, %s)",
                tuple(resp.values())
            )

        telegram_bot.sendtext("*{0}*\n_{1}_\nAttachment:{2}".format(
            resp['Title'],
            resp['Date'],
            resp['Attachment']
        ))

        con.commit()
    
    except psycopg2.errors.UniqueViolation as e:
        error = e.pgcode
        con.rollback()
        continue

con.commit()
con.close()
exit()