import lxml.html
import urllib.request
import psycopg2
import hashlib
import telegram_bot


content = urllib.request.urlopen("https://www.nrb.org.np/ofg/press.php").read()
doc = lxml.html.fromstring(content)
allRows = doc[1][1][2][2][1][:]
con = psycopg2.connect(
    database = "test",
    user = "refugee"
)

cur = con.cursor()
cur.execute('''
create table if not exists nrb_releases(
media_release varchar(500),
attachment varchar(200),
hashed char(64) unique
);''')

for i, tr in enumerate(allRows):
    resp = {}
    if len(tr) <= 1:
        continue 
    else:
        resp['Media Releases'] = tr[0].text_content()  
        if len(tr[1]) == 1 and tr[1][0].tag.__str__() == 'a':
            resp['Attachment'] = tr[1][0].get('href')
        resp['hashed'] = hashlib.sha256(tr.text_content().encode()).hexdigest()
        
        try:
            cur.execute(
                    "insert into nrb_releases values (%s, %s, %s)",
                    tuple(resp.values())
                )

            telegram_bot.sendtext("*{0}*\nAttachment:{1}".format(
                resp['Media Releases'],
                resp['Attachment']
            ))

            con.commit()
        
        except psycopg2.errors.UniqueViolation as e:
            error = e.pgcode
            print("FAIL!!")
            con.rollback()
            continue

con.commit()
con.close()
exit()
    