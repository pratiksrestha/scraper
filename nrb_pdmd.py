import lxml.html 
import urllib.request
import psycopg2
import hashlib
import telegram_bot


content = urllib.request.urlopen("https://www.nrb.org.np/pdm/pdmindex.php").read()
doc = lxml.html.fromstring(content)
home_content = doc.xpath("/html/body/table")

allRows =  home_content[0][2][2][1][0][0][0][0][0][0][:]

con = psycopg2.connect(
    database = "test",
    user = "kaazisaheb"
)

cur = con.cursor()
cur.execute('''
create table if not exists nrb_pdmd(
release varchar(500),
attachment varchar(200),
hashed char(64) unique
);''')


for i, tr in enumerate(allRows):
    
    if len(tr) > 1:
        continue 
    else:
        try:             
            
            innerRows = tr[0][0][:]
            
            for i,blockquote in enumerate(innerRows):
                resp = {}    
                for a in blockquote.findall('a'):             
                    resp['release'] = a.text_content().strip()
                    resp['Attachment'] = a.get('href')
                    resp['hashed'] = hashlib.sha256(tr.text_content().encode()).hexdigest()
                    
                
                for i,tr in enumerate(blockquote[0]):
                    tr = tr[0][0]
                    if tr[0].tag.__str__() == "a":
                        resp['release'] = tr.text_content().strip()
                        resp['Attachment'] = "https://www.nrb.org.np/pdm/" +tr[0].attrib['href']
                        resp['hashed'] = hashlib.sha256(tr.text_content().encode()).hexdigest()             
                        
        except IndexError as e:
            try:
                tr = tr[0]
                
                if tr[0].tag.__str__() == "a":
                    resp['release'] = tr.text_content().strip()
                    resp['Attachment'] = tr[0].attrib['href']
                    resp['hashed'] = hashlib.sha256(tr.text_content().encode()).hexdigest()
                    
                elif tr[0][0].tag.__str__() == "a":
                    resp['release'] = tr.text_content().strip()
                    resp['Attachment'] = tr[0][0].get('href')
                    resp['hashed'] = hashlib.sha256(tr.text_content().encode()).hexdigest()                
            except IndexError as e:
                continue
        if bool(resp):
            try:
                cur.execute(
                        "insert into nrb_study_reports values (%s, %s, %s)",
                        tuple(resp.values())
                    )

                telegram_bot.sendtext("*{0}*\nAttachment:\n{1}".format(
                    resp['release'],
                    resp['Attachment']
                ))

                con.commit()
            
            except psycopg2.errors.UniqueViolation as e:
                error = e.pgcode
                con.rollback()
                continue

con.commit()
con.close()
exit()
