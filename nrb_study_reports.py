import lxml.html
import urllib.request
import psycopg2
import hashlib
import telegram_bot


content = urllib.request.urlopen("https://www.nrb.org.np/red/publica.php?tp=study_reports&vw=20").read()
doc = lxml.html.fromstring(content)
allRows = doc[1][1][3][2][2][:]
con = psycopg2.connect(
    database = "test",
    user = "kaazisaheb"
)

cur = con.cursor()
cur.execute('''
create table if not exists nrb_study_reports(
study_reports varchar(500),
attachment varchar(200),
hashed char(64) unique
);''')

for i, tr in enumerate(allRows):
    resp = {}
    if len(tr) <= 1:
        continue 
    else:
        resp['Reports'] = tr[0].text_content().strip()  
        if len(tr[1]) == 1 and tr[1][0].tag.__str__() == 'a':
            resp['Attachment'] = "https://www.nrb.org.np/red/" + tr[1][0].get('href') 
        resp['hashed'] = hashlib.sha256(tr.text_content().encode()).hexdigest()
        # print(resp)
        # exit()
        
        try:
            cur.execute(
                    "insert into nrb_study_reports values (%s, %s, %s)",
                    tuple(resp.values())
                )

            telegram_bot.sendtext("*{0}*\nAttachment:\n{1}".format(
                resp['Reports'],
                resp['Attachment']
            ))

            con.commit()
        
        except psycopg2.errors.UniqueViolation as e:
            error = e.pgcode
            con.rollback()
            continue

con.commit()
con.close()
exit()
    