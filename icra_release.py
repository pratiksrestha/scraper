import lxml.html
import urllib.request
import psycopg2
import hashlib
import telegram_bot


content = urllib.request.urlopen("https://www.icranepal.com/releases/").read()
doc = lxml.html.fromstring(content)
home_content = doc.xpath('//*[@id="searchResult"]')
data = home_content[0][:]

con = psycopg2.connect(
    database = "test",
    user = "kaazisaheb"
)

cur = con.cursor()
cur.execute('''
create table if not exists icra_release(
release varchar(500),
attachment varchar(200),
hashed char(64) unique
);''')

for i , li in enumerate(data):
    resp = {}
    if li[0].tag.__str__() == "a":
        resp['releases'] = li[0].text_content().strip()
        resp['Attachment'] = li[0].get('href')
        resp['hashed'] = hashlib.sha256(li.text_content().encode()).hexdigest()
    if(bool(resp)):
        try:
            cur.execute(
            "insert into icra_release values (%s, %s, %s)",
                tuple(resp.values())
                )

            telegram_bot.sendtext("*{0}*\nAttachment:\n{1}".format(
                resp['releases'],
                resp['Attachment']
                ))

            con.commit()
            
        except psycopg2.errors.UniqueViolation as e:
            error = e.pgcode
            con.rollback()
            continue

con.commit()
con.close()
exit()
    